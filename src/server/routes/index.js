const express    = require("express");
const controller = require("../controllers");
const routes     = express.Router();

routes.route("/").get(controller.getFamilies);
routes.route("/all").get(controller.getAllFamilies);
routes.route("/phone").get(controller.getFamiliesPhone);
routes.route("/family").get(controller.getFamily);
routes.route("/new").post(controller.addFamily);
routes.route("/update").put(controller.updateFamily);
routes.route("/delete").delete(controller.deleteFamily);

module.exports = routes;