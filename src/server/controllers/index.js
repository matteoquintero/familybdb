const model = require("../models");

/**
 * @function getFamilies
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @returns {void}
 */

async function getFamilies(req, res) {
  const query  = req.query;

  if (!query.param) {
    res.status(422).json({
      error: true,
      data: "Missing required parameter"
    });

    return;
  }

  try {

    const result = await model.getFamilies(req.query);
    res.json({ success: true, data: result });

  } catch (err) {
    res.status(500).json({ success: false, error: "Unknown error."});
  }
}


/**
 * @function getFamiliesPhone
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @returns {void}
 */

async function getFamiliesPhone(req, res) {
  const query  = req.query;

  if (!query.param) {
    res.status(422).json({
      error: true,
      data: "Missing required parameter"
    });

    return;
  }

  try {

    const result = await model.getFamiliesPhone(req.query);
    res.json({ success: true, data: result });

  } catch (err) {
    res.status(500).json({ success: false, error: "Unknown error."});
  }
}

/**
 * @function getAllFamilies
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @returns {void}
 */

async function getAllFamilies(req, res) {
  const query  = req.query;

  try {

    const result = await model.getAllFamilies(req.query);
    res.json({ success: true, data: result });

  } catch (err) {
    res.status(500).json({ success: false, error: "Unknown error."});
  }
}



/**
 * @function getFamily
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @returns {void}
 */

async function getFamily(req, res) {
  const query  = req.query;

  if (!query.id) {
    res.status(422).json({
      error: true,
      data: "Missing required parameter"
    });

    return;
  }

  try {

    const result = await model.getFamily(req.query);
    res.json({ success: true, data: result });

  } catch (err) {
    res.status(500).json({ success: err, error: "Unknown error."});
  }
}


/**
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @returns {void}
 */

async function addFamily(req, res) {

  const body = req.body;

  if (!body.fullname || !body.phone || !body.date || (!body.father || !body.mother) ) {
    res.status(422).json({
      error: true,
      data: "Completa todos los campos"
    });
    return;
  }

  try {

    const result = await model.insertFamily(body.fullname, body.phone, body.date, body.father, body.mother);
    res.json({ 
      success: true, 
      data: {
        id:     result.body._id,
        phone: body.phone,
        fullname:  body.fullname,
        date:  body.date,
        father:  body.father,
        mother:  body.mother
      } 
    });

  } catch (err) {
    res.status(500).json({ success: false, error: "Unknown error."});
  }

}

/**
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @returns {void}
 */

async function updateFamily(req, res) {

  const body = req.body;

  if (!body.fullname || !body.phone || !body.date || (!body.father || !body.mother)) {
    res.status(422).json({
      error: true,
      data: "Completa todos los campos"
    });
    return;
  }

  try {

    const result = await model.updateFamily(body.fullname, body.phone, body.date, body.father, body.mother);
    res.json({ 
      success: true, 
      data: {
        id:     result.body._id,
        phone: body.phone,
        fullname:  body.fullname,
        date:  body.date,
        father:  body.father,
        mother:  body.mother
      } 
    });

  } catch (err) {
    res.status(500).json({ success: false, error: "Unknown error."});
  }

}

/**
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @returns {void}
 */

async function deleteFamily(req, res) {

  const body = req.body;

  if (!body.id) {
    res.status(422).json({
      error: true,
      data: "Completa todos los campos"
    });
    return;
  }

  try {

    const result = await model.deleteFamily(body.id);
    res.json({ 
      success: true, 
      data: {
        id:result.body._id,
      } 
    });

  } catch (err) {
    res.status(500).json({ success: false, error: "Unknown error."});
  }

}

module.exports = {
  getFamilies,
  addFamily,
  updateFamily,
  deleteFamily,
  getFamily,
  getAllFamilies,
  getFamiliesPhone
};