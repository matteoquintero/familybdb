const { esclient, index, type } = require("../../elastic");



async function getFamily(req) {

  const query = {
    query: {
      match: {
        id: {
          query: req.id,
          operator: "and",
          fuzziness: "auto"
        }
      }
    }
  }

  const { body: { hits } } = await esclient.search({
    from:  req.page  || 0,
    size:  req.limit || 100,
    index: index, 
    type:  type,
    body:  query
  });

  const results = hits.total.value;
  const results = req.id;
  const values  = hits.hits.map((hit) => {
    return {
      id:     hit._id,
      fullname:  hit._source.fullname,
      phone: hit._source.phone,
      date: hit._source.date,
      father: hit._source.father,
      mother: hit._source.mother,
    }
  });

  return {
    results,
    values
  }

}

async function getFamilies(req) {

  const query = {
    query: {
      match: {
        fullname: {
          query: req.param,
          operator: "and",
          fuzziness: "auto"
        }
      }
    }
  }

  const { body: { hits } } = await esclient.search({
    from:  req.page  || 0,
    size:  req.limit || 100,
    index: index, 
    type:  type,
    body:  query
  });

  const results = hits.total.value;
  const values  = hits.hits.map((hit) => {
    return {
      id:     hit._id,
      fullname:  hit._source.fullname,
      phone: hit._source.phone,
      date: hit._source.date,
      father: hit._source.father,
      mother: hit._source.mother,
    }
  });

  return {
    results,
    values
  }

}

async function getFamiliesPhone(req) {

  const query = {
    query: {
      match: {
        phone: {
          query: req.param,
          operator: "and",
          fuzziness: "auto"
        }
      }
    }
  }

  const { body: { hits } } = await esclient.search({
    from:  req.page  || 0,
    size:  req.limit || 100,
    index: index, 
    type:  type,
    body:  query
  });

  const results = hits.total.value;
  const values  = hits.hits.map((hit) => {
    return {
      id:     hit._id,
      fullname:  hit._source.fullname,
      phone: hit._source.phone,
      date: hit._source.date,
      father: hit._source.father,
      mother: hit._source.mother,
    }
  });

  return {
    results,
    values
  }

}

async function getAllFamilies(req) {


  const { body: { hits } } = await esclient.search({
    from:  req.page  || 0,
    size:  req.limit || 100,
    index: index, 
    type:  type,
  });

  const results = hits.total.value;
  const values  = hits.hits.map((hit) => {
    return {
      id:     hit._id,
      fullname:  hit._source.fullname,
      phone: hit._source.phone,
      date: hit._source.date,
      father: hit._source.father,
      mother: hit._source.mother,
    }
  });

  return {
    results,
    values
  }

}
async function insertFamily(fullname, phone, date, father, mother ) {
  return esclient.index({
    index,
    type,
    body: {
      fullname,
      phone,
      date,
      father,
      mother
    }
  })
}

async function updateFamily(fullname, phone, date, father, mother) {
  return esclient.update({
    index,
    type,
    id: id,
    body: {
      fullname,
      phone,
      date,
      father,
      mother
    }
  })
}

async function deleteFamily(id) {
  return esclient.delete({
    index,
    type,
    id: id
  })
}

module.exports = {
  getFamilies,
  insertFamily,
  updateFamily,
  deleteFamily,
  getAllFamilies,
  getFamily,
  getFamiliesPhone
}