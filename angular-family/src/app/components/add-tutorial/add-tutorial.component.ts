import { Component, OnInit } from '@angular/core';
import { TutorialService } from 'src/app/services/tutorial.service';

@Component({
  selector: 'app-add-tutorial',
  templateUrl: './add-tutorial.component.html',
  styleUrls: ['./add-tutorial.component.css']
})
export class AddTutorialComponent implements OnInit {
  tutorial = {
    fullname: '',
    phone: '',
    date: '',
    mother: '',
    father: ''
  };
  submitted = false;
  error = '';

  constructor(private tutorialService: TutorialService) { }

  ngOnInit(): void {
  }

  saveTutorial(): void {

    if (this.tutorial.phone !== ''){

    this.tutorialService.phone(this.tutorial.phone)
      .subscribe(
        datax => {
          let duplicated = false;
          if (datax.data.results > 1){
            // tslint:disable-next-line: prefer-for-of
            for (let index = 0; index < datax.data.values.length; index++) {
              const element = datax.data.values[index];
              if ( element.phone === this.tutorial.phone){
                duplicated = true;
              }
            }
          }

          if (!duplicated) {

            const data = {
              fullname: this.tutorial.fullname,
              phone: this.tutorial.phone,
              date: this.tutorial.date,
              mother: this.tutorial.mother,
              father: this.tutorial.father
            };

            this.tutorialService.create(data)
              .subscribe(
                response => {
                  console.log(response);
                  this.submitted = true;
                  this.error = '';
                },
                error => {
                  this.error = 'Completa todos los campos';
                  console.log(this.error);
                });
          }else{
            this.error = 'Ya esta en nuestra base datos el familiar';
          }

        },
        error => {
          console.log(error);
    });

    }else{
      this.error = 'Completa todos los campos';
    }

  }

  newTutorial(): void {
    this.submitted = false;
    this.tutorial = {
      fullname: this.tutorial.fullname,
      phone: this.tutorial.phone,
      date: this.tutorial.date,
      mother: this.tutorial.mother,
      father: this.tutorial.father
    };
  }

}
